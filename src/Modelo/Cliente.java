package Modelo;

import java.io.Serializable;
import java.util.ArrayList;

public class Cliente implements Serializable {
    // Atributos
    private String rut;
    private String nombre;
    private String direccion;
    private String telefono;
    private boolean activo;
    private ArrayList<Arriendo> arriendos;

    //Constructor
    public Cliente(String rut,String nom, String tel, String dir){
        this.rut = rut;
        this.nombre = nom;
        this.direccion = dir;
        this.telefono = tel;
        this.arriendos = new ArrayList<Arriendo>();
        // TODO: revisar
        this.activo = true;
    }
    //Metodo
    public String getRut() {
        return rut;
    }
    public String getNombre() {
        return nombre;
    }
    public String getDireccion() {
        return direccion;
    }
    public String getTelefono() {
        return telefono;
    }
    public boolean isActivo(){
            return activo;
    }
    public void setActivo() {
        this.activo = true;
    }
    public void setInactivo(){
        this.activo  = false;
    }
    public void addArriendo (Arriendo arriendo){
        // TODO: no es necesario hacer correciones?
        arriendos.add(arriendo);
    }
    public Arriendo[] getArriendosPorDevolver() {
        ArrayList<Arriendo> arriendoPorDevolver = new ArrayList<>();
        for (Arriendo arriendo: arriendos) {
            if (arriendo.getEstado() == EstadoArriendo.ENTREGADO) {
                arriendoPorDevolver.add(arriendo);
            }
        }
        return arriendoPorDevolver.toArray(new Arriendo[0]);
    }
}
