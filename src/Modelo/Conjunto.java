package Modelo;

import java.util.ArrayList;
public class Conjunto extends Equipo{
    // Atributos

    //Relaciones
    private ArrayList<Equipo> equipos;

    public Conjunto(long cod, String des) {
        super(cod, des);
        equipos = new ArrayList<>();

    }

    @Override
    public long getPrecioArriendoDia() {
        return equipos.stream().map(equipo -> equipo.getPrecioArriendoDia()).reduce(0L, Long::sum);
    }

    @Override
    public void addEquipo(Equipo equipo) {
        equipos.add(equipo);
    }
    @Override
    public int getNroEquipos() {
        return super.getNroEquipos();
    }
}