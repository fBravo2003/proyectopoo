package Modelo;

import java.time.LocalDate;

public class Credito extends Pago {
    //Atributos
private String codTransaccion;
private String numTarjeta;
private int nroCuotas;

// Constructor
public Credito (long monto, LocalDate fecha, String codTras, String numTarj, int nroCtos){
    super(monto, fecha);
    this.codTransaccion = codTras;
    this.numTarjeta = numTarj;
    this.nroCuotas = nroCtos;
}
//Metodos
    public String getCodTransaccion() {
        return codTransaccion;
    }

    public String getNumTarjeta() {
        return numTarjeta;
    }

    public int getNroCuotas() {
        return nroCuotas;
    }
}