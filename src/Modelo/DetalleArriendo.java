package Modelo;

import java.io.Serializable;

//parte de Walter Valdes
public class DetalleArriendo implements Serializable {
    //Atributos
    private long precioAplicado;
    private Equipo equipo;
    private Arriendo arriendo;
    //Constructor
    public DetalleArriendo(long precioAplicado, Equipo equipo, Arriendo arriendo) {
        this.precioAplicado = precioAplicado;
        this.equipo = equipo;
        equipo.addDetalleArriendo(this);
        this.arriendo = arriendo;
    }
    //Metodos

    public long getPrecioAplicado() {
        return precioAplicado;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public Arriendo getArriendo() {
        return arriendo;
    }
}
