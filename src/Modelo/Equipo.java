package Modelo;

import java.io.Serializable;
import java.util.ArrayList;

//Parte de Walter Valdes
public abstract class Equipo implements Serializable {
    //Atributos
    private long codigo;
    private String descripcion;
    private EstadoEquipo estado;
    private ArrayList<DetalleArriendo> detalles;
    private ArrayList<Conjunto> conjuntos;
    private ArrayList<Equipo>equipos;

    //Construtor
    public Equipo(Long cod, String desc){
        this.codigo = cod;
        this.descripcion = desc;
        // generar estado operativo
        estado = EstadoEquipo.OPERATIVO;
        this.detalles = new ArrayList<DetalleArriendo>();
        this.equipos = new ArrayList<>();
        this.conjuntos= new ArrayList<>();
    }

    //Metodo
    public long getCodigo() {
        return codigo;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public EstadoEquipo getEstado() {
        return estado;
    }
    public void setEstado(EstadoEquipo estado) {
        this.estado = estado;
    }
    public void addDetalleArriendo (DetalleArriendo detalle){
        detalles.add(detalle);
    }
    public boolean isArrendado(){
        if(detalles.isEmpty()){
            return false;
        }

        if(detalles.get(detalles.size()-1).getArriendo().getEstado() == EstadoArriendo.ENTREGADO){
            return true;
         }

        return false;
    }
    //utilizado por Conjunto y Implementos
    public abstract long getPrecioArriendoDia();
    //utilizados por Conjunto
    public void addEquipo (Equipo equipo){}
    public int getNroEquipos(){
        return 0;
    }
}