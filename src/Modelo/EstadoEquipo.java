package Modelo;

//Parte de Walter Valdes
public enum EstadoEquipo {
    OPERATIVO,
    EN_REPARACION,
    DADO_DE_BAJA
}