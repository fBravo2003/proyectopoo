package Modelo;

public class Implemento extends Equipo{
    //Atributos
    private long precioArriendoDia;
    //Constructor
    public Implemento (long cod, String desc, long precio){
        super(cod,desc);
        this.precioArriendoDia = precio;
    }
    //Metodos
    @Override
    public long getPrecioArriendoDia() {
        return precioArriendoDia;
    }
}
