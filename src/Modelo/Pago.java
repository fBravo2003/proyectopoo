package Modelo;

import java.io.Serializable;
import java.time.LocalDate;

public abstract class Pago implements Serializable {
    private long monto;
    private LocalDate fecha;

    // En el UML sale que se tiene el construtor acepta un parametro arriendo,
    // pero de acuerdo con la profesora fue solo un error
    public Pago(long monto, LocalDate fecha) {
        this.fecha = fecha;
        this.monto = monto;
    }

    public long getMonto() {
        return monto;
    }
    public LocalDate getFecha() {
        return fecha;
    }
}
