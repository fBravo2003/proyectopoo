package Vista;

import javax.swing.*;
import java.awt.event.*;
import Controlador.ControladorArriendoEquipos;
import Excepciones.EquipoException;
public class CreaEquipo extends JDialog {
    private JPanel panel1;
    private JButton OKButton;
    private JButton volverButton;
    private JTextField textFieldPrecio;
    private JTextField textFieldCodigo;
    private JLabel Imagen;
    private JLabel Codigo;
    private JLabel Implemento;
    private JTextField textFieldDescrpcion;
    private JLabel Descripcion;
    private JLabel PrecioarriendoDia;

    public CreaEquipo() {
        setContentPane(panel1);
        setModal(true);
        getRootPane().setDefaultButton(OKButton);
        OKButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOKButton();
            }
        });
        volverButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onvolverButton();
            }
        });
    }
    public static void display(){
        CreaEquipo dialog = new CreaEquipo();
        dialog.pack();
        dialog.setLayout(null);
        dialog.setLocationRelativeTo(null);
        dialog.setResizable(false);
        dialog.setVisible(true);
    }
    public void onOKButton(){
        String codigo = textFieldCodigo.getText();
        String descripcion = textFieldDescrpcion.getText();
        String precio = textFieldPrecio.getText();
        if (codigo.trim().equals("") || descripcion.trim().equals("") || precio.trim().equals("")){
            JOptionPane.showMessageDialog(this,"Ha ocurrido un error, no se puede ingresar el implemento","",JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            long cod = Long.parseLong(codigo);
            long pres = Long.parseLong(precio);
            ControladorArriendoEquipos.getInstance().creaImplemento(cod,descripcion,pres);
            JOptionPane.showMessageDialog(this,"Implemento creado satisfactoriamente","Mensaje",JOptionPane.INFORMATION_MESSAGE);
            textFieldCodigo.setText("");
            textFieldDescrpcion.setText("");
            textFieldPrecio.setText("");
        }catch (EquipoException e){
            JOptionPane.showMessageDialog(this,e.getMessage(),"Advertencia",JOptionPane.WARNING_MESSAGE);
        }catch (NumberFormatException | NullPointerException e) {
            JOptionPane.showMessageDialog(this,"Ha ocurrido un error, debe ingreser solo datos numericos","",JOptionPane.ERROR_MESSAGE);
        }
    }
    public void onvolverButton(){
        dispose();
    }
}