package Vista;

import Controlador.ControladorArriendoEquipos;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.*;

public class ListaClientes extends JDialog {
    private JPanel contentPane;
    private JButton buttonCancel;
    private JTable tableClientes;

    public ListaClientes() {
        setContentPane(contentPane);
        setModal(true);
        // getRootPane().setDefaultButton(buttonOK);

        buttonCancel.addActionListener(e -> dispose());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(
                e -> dispose(),
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        // Obtener los datos del controlador
        String[][] datosClientes = ControladorArriendoEquipos.getInstance().listaClientes();
        String[] titulos = {"Rut", "Nombre", "Direccion", "Telefono", "Estado", "Nro. Arr. Pdtes"};
        TableModel tableModel = new DefaultTableModel(datosClientes, titulos);
        tableClientes.setModel(tableModel);

    }

    public static void display() {
        ListaClientes dialog = new ListaClientes();
        dialog.pack();
        dialog.setVisible(true);
    }
}
