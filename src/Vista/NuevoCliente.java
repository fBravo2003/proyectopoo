package Vista;

import Controlador.ControladorArriendoEquipos;
import Excepciones.ClienteException;

import javax.swing.*;
import java.awt.event.*;

public class NuevoCliente extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nombreTextField;
    private JTextField rutTextField;
    private JTextField telefonoTextField;
    private JTextField direccionTextField;

    public NuevoCliente() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        String nombre = nombreTextField.getText();
        String rut = rutTextField.getText();
        String telefono = telefonoTextField.getText();
        String direccion = direccionTextField.getText();

        if (!validaRut(rut)) {
            JOptionPane.showMessageDialog(this, "El rut ingresado es invalido", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (nombre.trim().equals("") || telefono.trim().equals("") || direccion.trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Todos los parametros deben tener información", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            ControladorArriendoEquipos.getInstance().creaCliente(formatearRUT(rut), nombre, direccion, telefono);
            JOptionPane.showMessageDialog(this, "Se ha creado un cliente exitosamente", "Exito creando cliente", JOptionPane.INFORMATION_MESSAGE);
        } catch (ClienteException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void onCancel() {
        dispose();
    }

    private boolean validaRut(String rut) {
        if (rut.equals("")) {
            return false;
        }
        // Para validar el rut se toman todos lso digitos (sin el digito verificador)
        // cada digito de multiplica (en orden inverso) con uno de los siguente numero [2,3,4,5,6,7]
        // luego se suma y se calcula el modulo 11 de lo anterior, el resultado es el digito verificador
        // si el digito verficador
        // (Nota: si el resultado es 10 se remplaza el numero con un K)

        // Eliminar . y -
        rut = rut.replace(".", "");
        rut = rut.replace("-", "");
        String rutSinDigito = rut.substring(0, rut.length() - 1);
        int digitoVerificador;

        // Verificar que el rut sean solo datos numericos
        try {
            if (rut.charAt(rut.length() - 1) == 'K' || rut.charAt(rut.length() - 1) == 'k') {
                digitoVerificador = 10;
            } else {
                digitoVerificador = Integer.parseInt(rut.charAt(rut.length() - 1) + "");
            }
        } catch (Exception e) {
            return false;
        }

        int sumadorRut = 0;
        for (int i = rutSinDigito.length() - 1; i >= 0; i--) {
            int digito;
            try {
                digito = Integer.parseInt(rutSinDigito.charAt(i) + "");
            } catch (NumberFormatException e) {
                return false;
            }

            // Se puede calcular el numero de la secuencia usadando la posicion y el modulo 0
            // Si el primer digito la posicion es 0 (en sentido inverso) el modulo de 6 es 0 sumando 2 es 2
            // con el resto de numeros va a continuar aumentando en 1 hasta llegar al 6 donde vuelve a 0
            // de ese modo se tiene el minimo de 2 y maximo de 7, con una vuelta cada 6 digitos
            int multiplicaRut = ((rutSinDigito.length() - i - 1) % 6) + 2;

            sumadorRut += digito * multiplicaRut;
        }

        if (11 - (sumadorRut % 11) == digitoVerificador) {
            return true;
        }
        return false;
    }

    private String formatearRUT(String rut) {
        int cont = 0;
        String format;
        rut = rut.replace(".", "");
        rut = rut.replace("-", "");
        format = "-" + rut.substring(rut.length() - 1);
        for (int i = rut.length() - 2; i >= 0; i--) {
            format = rut.substring(i, i + 1) + format;
            cont++;
            if (cont == 3 && i != 0) {
                format = "." + format;
                cont = 0;
            }
        }
        return format;
    }

    public static void display() {
        NuevoCliente dialog = new NuevoCliente();
        dialog.pack();
        dialog.setVisible(true);
    }
}
