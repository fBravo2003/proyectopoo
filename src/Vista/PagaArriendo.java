package Vista;

import Controlador.ControladorArriendoEquipos;
import Excepciones.ArriendoException;
import Modelo.Credito;

import javax.swing.*;
import java.awt.event.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PagaArriendo extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField buscarArriendo;
    private JButton buscarArriendoButton;
    private JRadioButton contadoRadioButton;
    private JRadioButton debitoRadioButton;
    private JRadioButton creditoRadioButton;
    private JLabel Estado;
    private JLabel rutCliente;
    private JLabel nombreCLiente;
    private JLabel montoTotal;
    private JLabel montoPagado;
    private JLabel montoAdeudado;
    private JTextField montoTextField;
    private JTextField numTransaccionTextField;
    private JTextField numTarjetaTextField;
    private JTextField numCuotasTextField;
    private JLabel fechaHoy;

    private JLabel numtrans;
    private JLabel numtar;
    private JLabel numcuot;


    public PagaArriendo() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        actualizaTextField(false, false, false);
        fechaHoy.setText(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        contadoRadioButton.addActionListener(e -> actualizaTextField(false, false, false));
        debitoRadioButton.addActionListener(e -> actualizaTextField(true, true, false));
        creditoRadioButton.addActionListener(e -> actualizaTextField(true, true, true));

        buttonOK.addActionListener(e -> onOK());

        buscarArriendoButton.addActionListener(e -> buscarArriendo());

        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(
                e -> onCancel(),
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }
    private void actualizaTextField(boolean transaccion, boolean tarjeta, boolean coutas) {
        numTransaccionTextField.setEnabled(transaccion);
        numTarjetaTextField.setEnabled(tarjeta);
        numCuotasTextField.setEnabled(coutas);
        numtrans.setEnabled(transaccion);
        numtar.setEnabled(tarjeta);
        numcuot.setEnabled(coutas);
    }

    private void onOK() {
        String codigoStr = buscarArriendo.getText();
        String montoStr = montoTextField.getText();
        String NumeroDeTransaccion = numTransaccionTextField.getText();
        String NumeroDeTarjeta = numTarjetaTextField.getText();
        String NumeroDeCuotas = numCuotasTextField.getText();
        if (codigoStr.trim().equals("") || montoStr.trim().equals("")) {
            JOptionPane.showMessageDialog(this, "No se pueden dejar campos vacios", "Error", JOptionPane.ERROR_MESSAGE);
        }

        try {
            long codigo = Long.parseLong(codigoStr);
            long monto = Long.parseLong(montoStr);

            if (contadoRadioButton.isSelected()) {
                ControladorArriendoEquipos.getInstance().pagaArriendoContado(codigo, monto);
                JOptionPane.showMessageDialog(this, "Arriendo pagado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                return;
            }

            boolean checker = NumeroDeTarjeta.trim().equals("") || NumeroDeTransaccion.trim().equals("");

            if (debitoRadioButton.isSelected() && !checker) {
                ControladorArriendoEquipos.getInstance().pagaArriendoDebito(codigo,monto,NumeroDeTransaccion,NumeroDeTarjeta);
            } else if (creditoRadioButton.isSelected() && !checker) {
                int cuotas = Integer.parseInt(NumeroDeCuotas);
                ControladorArriendoEquipos.getInstance().pagaArriendoCredito(codigo,monto,NumeroDeTransaccion, NumeroDeTarjeta, cuotas);
            } else {
                JOptionPane.showMessageDialog(this, "Los parametros no pueden estar vacios", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            JOptionPane.showMessageDialog(this, "Arriendo pagado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (ArriendoException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Los campos de codigo, monto y cuotas deben ser numericos", "Error", JOptionPane.ERROR_MESSAGE);
        }

        buscarArriendo.setText("");
        montoTextField.setText("");
        numTransaccionTextField.setText("");
        numTarjetaTextField.setText("");
        numCuotasTextField.setText("");

        Estado.setText("");
        rutCliente.setText("");
        nombreCLiente.setText("");
        montoTotal.setText("");
        montoAdeudado.setText("");
        montoPagado.setText("");
    }
    private void buscarArriendo(){
        String CodigoDeArriendo = buscarArriendo.getText();
        if(!CodigoDeArriendo.isEmpty()){
            try {
                String[] consultaArriendo = ControladorArriendoEquipos.getInstance()
                        .consultaArriendoAPagar(Long.parseLong(CodigoDeArriendo));
                Estado.setText(consultaArriendo[1]);
                rutCliente.setText(consultaArriendo[2]);
                nombreCLiente.setText(consultaArriendo[3]);
                montoTotal.setText(consultaArriendo[4]);
                montoAdeudado.setText(consultaArriendo[5]);
                montoPagado.setText(consultaArriendo[6]);

            }catch(Exception e) {
                JOptionPane.showMessageDialog(this,"No existe arriendo","Advertencia",JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private void onCancel() {
        dispose();
    }

    public static void display(){
        PagaArriendo dialog = new PagaArriendo();
        dialog.pack();
        dialog.setVisible(true);
    }
}
