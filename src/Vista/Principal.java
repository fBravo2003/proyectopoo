package Vista;

import javax.swing.*;
import java.awt.event.*;
import Controlador.ControladorArriendoEquipos;
import Excepciones.ArriendoException;

public class Principal extends JDialog{
    private JButton salidaButton;
    private JButton guardarButton;
    private JButton abrirButton;
    private JPanel Contenedor;
    private JButton arriendoEquipoButton;
    private JButton nuevoClienteButton;
    private JButton nuevoImplementoButton;
    private JButton nuevoConjuntoButton;
    private JButton devuelveEquipoButton;
    private JButton pagaArriendoButton;
    private JButton listadoArriendosPagadosButton;
    private JButton listadoClientesButton;
    private JButton listadoEquiposButton;
    private JButton listadoPagosArriendoButton;
    private JButton detallesDeUnAButton;
    private JButton listadoArriendosButton;

    public Principal() {
        setContentPane(Contenedor);
        setModal(true);
        getRootPane().setDefaultButton(abrirButton);
        listadoPagosArriendoButton.addActionListener(e -> ListaPagosArriendo.display());
        listadoClientesButton.addActionListener(e -> ListaClientes.display());
        listadoArriendosButton.addActionListener(e -> ListadoDeArriendos.display());
        pagaArriendoButton.addActionListener(e -> PagaArriendo.display());
        nuevoImplementoButton.addActionListener(e -> CreaEquipo.display());
        nuevoClienteButton.addActionListener(e -> NuevoCliente.display());
        guardarButton.addActionListener(e -> onguardarButton());
        abrirButton.addActionListener(e -> cargarDatos());
        salidaButton.addActionListener(e -> dispose());
    }
   public void onguardarButton() {
       try {
           ControladorArriendoEquipos.getInstance().saveDatosSistemas();
           JOptionPane.showMessageDialog(this, "Datos guardados correctamente", "info", JOptionPane.INFORMATION_MESSAGE);
       } catch (ArriendoException ex) {
           JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
       }
   }

    public void cargarDatos() {
        try {
            ControladorArriendoEquipos.getInstance().readDatosSistema();
            JOptionPane.showMessageDialog(this, "Datos cargados correctamente", "info", JOptionPane.INFORMATION_MESSAGE);
        } catch (ArriendoException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }


    public static void main(String[] args) {
        Principal dialog = new Principal();
        dialog.pack();
        dialog.setLayout(null);
        dialog.setResizable(false);
        dialog.setVisible(true);
        System.exit(0);
    }
}
